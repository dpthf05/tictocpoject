<?php


namespace App\Daos\Tictoc;


class ChargeDAO extends BaseDAO
{
    public function selectCharge()
    {
        $sql = "
                    SELECT
                        caretype_1, -- 놀이 1시간 당 금액
                        caretype_2, -- 등하원 1시간 당 금액
                        caretype_3, -- 학습 1시간 당 금액
                        caretype_4, -- 창의미술 1시간 당 금액
                        caretype_5, -- 영어 1시간 당 금액
                        add_premium_1, -- 특기선생님일 경우 기본 1시간당 금액에 추가되는 프리미엄
                        add_premium_2, -- 보육선생님일 경우 기본 1시간당 금액에 추가되는 프리미엄
                        add_child, -- 돌봄아이 1명 초과 시 추가된 아이의 시간 당 금액
                        add_fare_support -- 2시간 미만 돌봄 시 이동지원금
                    FROM
                        tictoccroc_admin.charge_table
                ";

        return $this->query_row($this->app_db, $sql);
    }
}