<?php


namespace App\Daos\Tictoc;


class SalaryDAO extends BaseDAO
{
    public function select_salary($class)
    {
        $sql = "
                SELECT
                       teacher_type_price_1 AS care_type_price_0, -- 대학생 놀이 /등하원 1시간당 금액
                       care_type_price_1, -- 학습 1시간 당 금액
                       care_type_price_2, -- 창의미술 1시간 당 금액
                       care_type_price_3, -- 영어 1시간 당 금액
                       child_price,
                       add_fare_support
                FROM
                    tictoccroc_admin.salary_table
                WHERE class = ?
            ";

        return $this->query_row($this->app_db, $sql, array($class));
    }
}