<?php


namespace App\Daos;


use App\Daos\Tictoc\BaseDAO;

class ParentDAO extends BaseDAO
{
    public function select_cash_list($parents_idx)
    {
        $sql = "
            SELECT
                pc.idx,
            FROM
                tictoccroc_app.parents_cash AS pc
            WHERE
                pc.plus_contents = '0'
                AND pc.cash_flag = 'P'
                AND pc.confirm_flag = 'Y'
            ORDER BY pc.idx DESC
        ";
        return $this->query_list($this->app_db, $sql, array($parents_idx));
    }

    public function select_plus_point_list($parents_idx, $target_cash_arr)
    {
        $sql = "
                SELECT
                    idx,
                    point,
                    remain_point
                FROM
                    tictoccroc_app.parents_point_plus
                WHERE
                    parents_idx = ?
                    AND point_type = '4'
                    AND type_idx in ?
                    AND cancel_flag = 'N'
                    AND delete_flag = 'N'
                ";
        return $this->query_list($this->app_db, $sql, array($parents_idx, $target_cash_arr));
    }

    public function select_minus_point($parents_idx, $target_point_arr)
    {
        $sql = "
                    SELECT
                        SUM(point) AS used_point
                    FROM
                        tictoccroc_app.parents_point_minus
                    WHERE
                        parents_idx = ?
                        AND point_type = '0'
                        AND target_point_idx in ?
                        AND cancel_flag = 'N'
                        AND delete_flag = 'N'
                ";
        return $this->query_row($this->app_db, $sql, array($parents_idx, $target_point_arr));
    }
}