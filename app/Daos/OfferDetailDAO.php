<?php


namespace App\Daos\Tictoc;


class OfferDetailDAO extends BaseDAO
{
    public function select_offer_detail($offer_idx)
    {
        $sql = "
                    SELECT
                        child_cnt,
                        DATE_FORMAT(care_start_time, '%m월 %d일') AS care_date,
                        today_care_flag,
                        first_care_type,
                        first_care_time,
                        second_care_type,
                        second_care_time
                    FROM
                        tictoccroc_app.offer_detail
                    WHERE
                        offer_idx = ?
                    ORDER BY care_date
                ";
        return $this->query_list($this->app_db, $sql, array($offer_idx));
    }
}