<?php
namespace App\Daos\Tictoc;

class MatchingDAO extends BaseDAO
{
    public function select_remain_minute($matching_idx)
    {
        $sql = "
            SELECT  
                TIMESTAMPDIFF(MINUTE, NOW(), od.care_start_time) AS remain_minute
            FROM 
                tictoccroc_app.matching m
                JOIN tictoccroc_app.offer_detail od
                    ON od.idx = m.offer_detail_idx
            WHERE 
                  m.idx = ?
        ";

        return $this->query_row($this->app_db, $sql, array($matching_idx));
    }

    public function select_matching_detail_info($matching_idx)
    {
        $sql1 = "
                SELECT
                    m.idx AS matching_idx,
                    eai.teacher_type,
                    DATE_FORMAT(od.care_start_time,'%Y-%m-%d') AS care_date,
                    DATE_FORMAT(od.care_start_time,'%H:%i') AS care_start_time,
                    DATE_FORMAT(od.care_end_time,'%H:%i') AS care_end_time,
                    od.today_care_flag,
                    od.first_care_type,
                    od.first_care_time,
                    od.second_care_type,
                    od.second_care_time,
                    od.child_cnt
                FROM
                    tictoccroc_app.matching AS m
                    JOIN tictoccroc_app.offer_detail AS od
                        ON od.idx = m.offer_detail_idx
                    JOIN tictoccroc_app.offer_teacher AS ot
                        ON ot.offer_idx = m.offer_idx
                    JOIN tictoccroc_app.teacher AS t
                        ON t.idx = ot.teacher_idx
                    JOIN tictoccroc_app.education_applicant_info AS eai
                        ON eai.applicant_idx = ot.teacher_idx
                WHERE
                    m.idx = ?
                    AND ot.status = '50'
                ORDER BY ot.idx DESC LIMIT 0,1
            ";

        return $this->query_row($this->app_db, $sql1, array($matching_idx));
    }
}