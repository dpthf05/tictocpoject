<?php
namespace App\Daos\Tictoc;
require_once('App_query.php');

class BaseDAO extends App_query
{
    function __construct()
    {
        $CI =& get_instance();
        $CI->load->library('app_query');
        $this->app_db = $CI->load->database('app_db', true);
        $this->amdin_db = $CI->load->database('admin_db', true);
    }
}