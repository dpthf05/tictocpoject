<?php


namespace App\Services\Tictoc;


class CareService
{
    /**
     * 돌봄코드와 이름 매칭
     * @param $care_type
     * @return string
     */
    public function get_care_name_by_type($care_type): string
    {
        $care_names = array(
            "1" => "놀이",
            "2" => "등하원",
            "3" => "학습",
            "4" => "창의미술",
            "5" => "영어",
        );
        return in_array($care_names, $care_type) ? $care_names[$care_type] : "";
    }

    /**
     * 요청 가능 시간 체크?
     * @param $first_time
     * @param $second_time
     * @return bool
     */
    public function check_care_request_time($first_time, $second_time): bool
    {
        $result = FALSE;

        $request_time = (float)$first_time + (float)$second_time;

        if ($request_time <= 16.0) {
            $result = TRUE;
        }

        if ($request_time < 0.5 || (($request_time * 10) % 5) !== 0) {
            $result = FALSE;
        }

        return $result;
    }
}