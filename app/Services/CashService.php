<?php


namespace App\Services\Tictoc;


class CashService
{
    /**
     * 캐시충전 시 지급되는 보너스 포인트 금액
     * @param $point
     * @return int
     */
    public function calculate_bonus_point($point): int
    {
        $bonus = 0;

        switch ($point) {
            case "30000" :
            case "500000" :
                $bonus = 15000;
                break;
            case "300000" :
                $bonus = 6000;
                break;
            case "1000000" :
                $bonus = 50000;
                break;
            case "32" :
            case "1000" :
                $bonus = 100000;
                break;
            case "300" :
                $bonus = 30;
                break;
            default :
                break;
        }

        return $bonus;
    }
}