<?php


namespace App\Services\Tictoc;


class RefundService
{
    private $parentDAO;
    function __construct()
    {
        $this->parentDAO = new \App\Daos\ParentDAO();
    }

    /**
     * 캐시 환불금액 계산
     * @param $cal_data
     * @return array
     */
    public function calculate_refund($cal_data)
    {
        $used_bonus_point = 0;
        $target_point =0;
        $target_point_array = array();

        $cash_list = $this->parentDAO->select_cash_list($cal_data['parents_idx']);

        if (!empty($cash_list)) {
            $target_cash_arr = self::get_target_cash_idxs($cash_list, $cal_data['remain_cash']);

            if (!empty($target_cash_arr)) {
                $point_list = $this->parentDAO->select_plus_point_list($cal_data['parents_idx'], $target_cash_arr);

                if (!empty($point_list)) {
                    $target_point_info = self::get_target_point_arr($point_list);
                    $target_point = $target_point_info['target_point'];
                    $target_point_arr = $target_point_info['target_point_arr'];
                    $target_point_array = $target_point_info['target_point_array'];

                    // 사용한 보너스포인트
                    if (!empty($target_point_arr)) {
                        $cal_result = $this->parentDAO->select_minus_point($cal_data['parents_idx'], $target_point_arr);
                        $used_bonus_point = $cal_result !== NULL ? (int)$cal_result->used_point : 0;
                    }
                }
            }
        }

        $refund_cash = self::get_refund_cash($cal_data['remain_cash'], $used_bonus_point);

        return [
            'used_bonus_point' => $used_bonus_point,
            'refund_cash' => $refund_cash,
            'withdraw_cash' => $cal_data['remain_cash'],
            'withdraw_point' => $target_point - $used_bonus_point,
            'target_plus_point' => $target_point_array
        ];
    }

    private function get_refund_cash($remain_cash, $used_bonus_point)
    {
        $refund_cash = $remain_cash - $used_bonus_point;
        return ($refund_cash > 0) ? $refund_cash : 0;
    }

    private function get_target_point_arr($point_list)
    {
        $target_point = 0;  // 지급한 보너스포인트 합계
        $target_point_arr = array();
        $target_point_array = array();
        $target_point_cnt = 0;

        foreach ($point_list as $row) {
            $target_point_arr[] = (int)$row->idx;
            $target_point += $row->point;
            $target_point_array[$target_point_cnt]['plus_idx'] = $row->idx;
            $target_point_array[$target_point_cnt]['point'] = $row->point;
            $target_point_cnt++;
        }
        return [
            'target_point'=>$target_point,
            'target_point_arr'=>$target_point_arr,
            'target_point_array' => $target_point_array
        ];
    }

    private function get_target_cash_idxs($cash_list, $remain_cash): array
    {
        $target_cash = 0;
        $target_cash_arr = array();

        foreach ($cash_list as $row) {
            if ($target_cash > $remain_cash) {
                break;
            }
            $target_cash_arr[] = (int)$row->idx;
        }

        return $target_cash_arr;
    }
}