<?php


namespace App\Services\Tictoc;


class CouponService
{
    /**
     * 쿠폰사용조건 가능여부 확인
     * @param $coupons
     * @param int $min_price
     * @param int $first_care_type
     * @param int $second_care_type
     * @return string
     */
    public function check_coupon_validation($coupons, int $min_price, int $first_care_type, int $second_care_type): string
    {
        if (empty($coupons))
            return "Y";

        foreach ($coupons as $coupon) {
            $type = $coupon['type'];
            $detail = (int)$coupon['detail'];

            // A : 최소결제금액, B : 돌봄유형, C : 돌봄요일
            if ($type === 'A') {
                if ($min_price < $detail)
                    return "N";
            } else if ($type === 'B') {
                if (!self::includedInCareType($first_care_type, $detail) &&
                    !self::includedInCareType($second_care_type, $detail))
                    return "N";
            } else if ($type === 'C') {
                if (!self::includedInCareDay($detail))
                    return "N";
            } else {
                return "N";
            }
        }
        return "Y";
    }

    /**
     * 돌봄 타입에 포함되는지 여부
     * @param $target
     * @param $care_types
     * @return bool
     */
    private function includedInCareType($target, $care_types) : bool
    {
        return in_array($target, explode(',', $care_types));
    }

    /**
     * 돌봄 요일에 포함되는지 여부
     * @param string $care_days
     * @return bool
     */
    private function includedInCareDay(string $care_days): bool
    {
        return in_array(date('w'), explode(',', $care_days));
    }
}