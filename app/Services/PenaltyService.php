<?php


namespace App\Services\Tictoc;


use App\Daos\MatchingDAO;

class PenaltyService
{
    private $matchingDAO;

    function __construct()
    {
        $this->matchingDAO = new MatchingDAO();
    }

    /**
     * 위약금,배상금,보상금 계산
     * @param $matching_idx
     * @return int
     */
    public function calculate_penalty_cost($matching_idx): int
    {
        $result = $this->matchingDAO->select_remain_minute($matching_idx);
        $remain_minute = $result->remain_minute;
        $TREE_HOUR_TO_MINUTE = 60 * 3;
        $TWO_DAYS_TO_MINUTE = 60 * 24 * 2;

        if ($remain_minute < 0) {
            $penalty_cost = $this->service_price($matching_idx)['scheduled_price'];
        } else if ($remain_minute < $TREE_HOUR_TO_MINUTE) {
            $penalty_cost = 20000;
        } else if ($remain_minute < $TWO_DAYS_TO_MINUTE) {
            $penalty_cost = 10000;
        } else {
            $penalty_cost = 0;
        }

        return $penalty_cost;
    }
}