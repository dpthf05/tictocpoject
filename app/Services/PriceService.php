<?php


namespace App\Services\Tictoc;

use App\Daos\Tictoc\ChargeDAO;
use App\Daos\Tictoc\MatchingDAO;
use App\Daos\Tictoc\OfferDetailDAO;

class PriceService
{
    private $matchingDAO;
    private $chargeDAO;
    private $offerDetailDAO;
    function __constructor()
    {
        $this->matchingDAO = new MatchingDAO();
        $this->chargeDAO = new ChargeDAO();
        $this->offerDetailDAO = new OfferDetailDAO();
    }
    /**
     * 서비스 이용료 계산
     * @param $matching_idx
     * @return array
     */
    public function get_service_price_info($matching_idx): array
    {
        $matchingInfo = $this->matchingDAO->select_matching_detail_info($matching_idx);

        if (!empty($matchingInfo)) {
            $teacher_type = $matchingInfo->teacher_type;
            $today_care_flag = $matchingInfo->today_care_flag;
            $first_type = $matchingInfo->first_care_type;
            $first_time = $matchingInfo->first_care_time;
            $second_type = $matchingInfo->second_care_type;
            $second_time = $matchingInfo->second_care_time;
            $child_cnt = $matchingInfo->child_cnt;

            $charge_info = $this->chargeDAO->selectCharge();

            $today_care = 0;
            $care_service = 0;
            $traffic_support = 0;

            // 2시간 미만에 따른 선생님 이동지원금 추가
            if ($first_time < 2 && $second_type === '') {
                $traffic_support += $charge_info->add_fare_support;
            }
            // 영어돌봄이 있다면 이동지원금 미지급
            if ($first_type === '5' || $second_type === '5') {
                $traffic_support = 0;
            }
            if ($today_care_flag === 'Y') {
                $today_care += 10000;
            }

            // 아이가 2명 이상일 경우 2명째부터 별도 금액으로 계산
            if ($child_cnt > 1) {
                $care_service = self::getPriceWhenOverOneChild($care_service, $first_type, $first_time,
                    $second_type, $second_time, $child_cnt, $charge_info->add_child);
            }
            // 첫번째 유형의 서비스금액 계산
            if ($first_type !== '') {
                $care_service += self::get_service_price($first_type, $first_time, $teacher_type, $charge_info);
            }
            // 두번째 유형의 서비스금액 계산
            if ($second_type !== '') {
                $care_service += self::get_service_price($second_type, $second_time, $teacher_type, $charge_info);
            }

            $scheduled_price = $care_service + $traffic_support + $today_care;
        }

        return [
            'today_care' => empty($today_care) ? 0 : $today_care,
            'care_service' => empty($care_service) ? 0 : $care_service,
            'traffic_support' => empty($traffic_support) ? 0 : $traffic_support,
            'scheduled_price' => empty($scheduled_price) ? 0 : $scheduled_price,
        ];
    }

    /**
     * 서비스 금액 계산
     * @param $type
     * @param $time
     * @param $teacher_type
     * @param $charge_info
     * @return float|int
     */
    private function get_service_price($type, $time, $teacher_type, $charge_info)
    {
        $standard = 0;

        switch ($type) {
            case '1' :
                $standard = ($teacher_type === '2') ? ($charge_info->caretype_1 + $charge_info->add_premium_2) : $charge_info->caretype_1;
                break;
            case '2' :
                $standard = ($teacher_type === '2') ? ($charge_info->caretype_2 + $charge_info->add_premium_2) : $charge_info->caretype_2;
                break;
            case '3' :
                $standard = $charge_info->caretype_3;
                break;
            case '4' :
                $standard = $charge_info->caretype_4;
                break;
            case '5' :
                $standard = $charge_info->caretype_5;
                break;
            default :
                break;
        }
        return $standard * $time;
    }

    private function getPriceWhenOverOneChild($care_service, $first_type, $first_time,
                                             $second_type, $second_time, $child_cnt, int $add_child)
    {
        $policy_diff = time() > strtotime('2021-06-28 00:00:00');

        if ($policy_diff && $first_type === '4')
            $care_service += ($first_time * 10000) * ($child_cnt - 1);
        else
            $care_service += ($first_time * $add_child) * ($child_cnt - 1);

        if ($policy_diff && $second_type === '4')
            $care_service += ($second_time * 10000) * ($child_cnt - 1);
        else
            $care_service += ($second_time * $add_child) * ($child_cnt - 1);

        return $care_service;
    }


    public function get_predicted_price_info($query_data)
    {
        $offerDetailList = $this->offerDetailDAO->select_offer_detail($query_data['offer_idx']);
        if (empty($offerDetailList))
            return NULL;

        $result_cnt = 0;
        $result = array();
        $service_price = $this->chargeDAO->selectCharge();

        foreach ($offerDetailList as $offer) {
            $result[$result_cnt]['care_date'] = $offer->care_date;
            $result[$result_cnt]['min_price'] = 0;
            $result[$result_cnt]['max_price'] = 0;

            $today_care_flag = $offer->today_care_flag;
            $first_type = $offer->first_care_type;
            $first_time = $offer->first_care_time;
            $second_type = $offer->second_care_type;
            $second_time = $offer->second_care_time;
            $child_cnt = $offer->child_cnt;

            $care_service = 0;
            $traffic_support = 0;
            $today_care = 0;
            $premium = 0;

            // 2시간 미만에 따른 선생님 이동지원금 추가
            if ($first_time < 2 && $second_type === '') {
                $traffic_support += $service_price->add_fare_support;
            }
            // 영어돌봄이 있다면 이동지원금 미지급
            if ($first_type === '5' || $second_type === '5') {

                $traffic_support = 0;
            }
            // 아이가 2명 이상일 경우 2명째부터 별도 금액으로 계산
            if ($child_cnt > 1) {
                $care_service = self::getPriceWhenOverOneChild($care_service, $first_type, $first_time,
                    $second_type, $second_time, $child_cnt, $service_price->add_child);
            }
            if ($today_care_flag === 'Y') {
                $today_care += 10000;
            }
            // 첫번째 유형의 서비스금액 계산
            if ($first_type !== '') {
                $premium += self::get_premium_price($first_type, $first_time, $service_price);
                $care_service += self::get_standard_price($first_type, $service_price) * $first_time;
            }
            // 두번째 유형의 서비스금액 계산
            if ($second_type !== '') {
                $premium += self::get_premium_price($second_type, $second_time, $service_price);
                $care_service += self::get_standard_price($second_type, $service_price) * $second_time;
            }

            $result[$result_cnt]['min_price'] = $care_service + $traffic_support + $today_care;
            $result[$result_cnt]['max_price'] = $care_service + $traffic_support + $today_care + $premium;

            $result_cnt++;
        }

        return $result;
    }

    /**
     * 프리미엄 가격 조회
     * @param $type
     * @param $time
     * @param $service_price
     * @return int
     */
    private function get_premium_price($type, $time, $service_price): int
    {
        return in_array($type, ['1','2']) ? $service_price->add_premium_2 * $time : 0;
    }

    /**
     * 기본 가격 조회
     * @param $type
     * @param $price
     * @return int
     */
    private function get_standard_price($type, $price): int
    {
        $standard = 0;

        switch ($type) {
            case '1' :
                $standard = $price->caretype_1;
                break;
            case '2' :
                $standard = $price->caretype_2;
                break;
            case '3' :
                $standard = $price->caretype_3;
                break;
            case '4' :
                $standard = $price->caretype_4;
                break;
            case '5' :
                $standard = $price->caretype_5;
                break;
            default :
                break;
        }
        return $standard;
    }
}