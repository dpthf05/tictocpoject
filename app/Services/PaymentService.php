<?php


namespace App\Services\Tictoc;


class PaymentService
{
    /**
     * 결제금액 정보 조회
     * @param $scheduled_price
     * @param $remain_coupon
     * @param $remain_point
     * @param $remain_cash
     * @return array
     */
    public function get_payment_info($scheduled_price, $remain_coupon, $remain_point, $remain_cash): array
    {
        if ($scheduled_price > 0) {
            // 쿠폰 결제금액 계산
            $final_price = 0;
            if ($remain_coupon > 0) {
                $coupon_price = $remain_coupon > $scheduled_price ? $scheduled_price : $remain_coupon;;
                $final_price -= $coupon_price;
            }
            // 포인트 결제금액 계산
            if ($remain_point > 0 && $remain_coupon < 1) {
                $limit_price = (int)($scheduled_price * 0.7);
                $result['point_price'] = $remain_point > $limit_price?$limit_price:$remain_point;
                $final_price -= $result['point_price'];
            }

            // 캐시 결제금액 계산
            if ($remain_cash > 0) {
                $cash_price = $remain_cash > $final_price ? $final_price : $remain_cash;
                $final_price -= $cash_price;
            }

            // 카드 결제금액 게산
            if ($final_price > 200)
                $card_price = $final_price;
        }

        return array(
            'card_price' => empty($card_price) ? 0 : $card_price,
            'cash_price' => empty($cash_price) ? 0 : $cash_price,
            'point_price' => empty($point_price) ? 0 : $point_price,
            'coupon_price' => empty($coupon_price) ? 0 : $coupon_price,
        );
    }
}