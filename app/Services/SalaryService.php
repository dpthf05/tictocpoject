<?php


namespace App\Services\Tictoc;


use App\Daos\Tictoc\SalaryDAO;

class SalaryService
{
    private $salaryDAO;
    function __constructor()
    {
        $this->salaryDAO = new SalaryDAO();
    }

    /**
     * 예상되는 선생님 활동비 계산
     * @param $type
     * @param $first_type
     * @param $first_time
     * @param $second_type
     * @param $second_time
     * @param $child_cnt
     * @return float|int
     */
    public function calculate_expected_salary($type, $first_type, $first_time,
                                              $second_type, $second_time, $child_cnt)
    {
        $result = 0;

        if (!in_array($type, ['min', 'max']))
            return $result;

        $class = ($type === 'min') ? '1' : '3';
        $salary_price_info = $this->salaryDAO->select_salary($class);

        $care_service = 0;
        $traffic_support = 0;
        $add_child_price = 0;

        // 2시간 미만에 따른 선생님 이동지원금 추가
        if ($first_time < 2 && $second_type === '') {
            $traffic_support += $salary_price_info->add_fare_support;
        }
        // 영어돌봄이 있다면 이동지원금 미지급
        if ($first_type === '5' || $second_type === '5') {
            $traffic_support = 0;
        }
        // 아이가 2명 이상일 경우 2명째부터 별도 금액으로 계산
        if ($child_cnt > 1) {
            $add_child_price += (($first_time + $second_time) * $salary_price_info->child_price) * ($child_cnt - 1);
        }
        // 첫번째 유형의 급여금액 계산
        if ($first_type !== '') {
            $care_service += self::get_salary($first_type, $first_time, $salary_price_info);
        }
        // 두번째 유형의 급여금액 계산
        if ($second_type !== '') {
            $care_service += self::get_salary($second_type, $second_time, $salary_price_info);
        }

        return $care_service + $traffic_support + $add_child_price;
    }

    private function get_salary($type, $time, $salary_price)
    {
        $standard = 0;
        switch ($type) {
            case '2':
            case '1' :
                $standard = $salary_price->care_type_price_0;
                break; // 놀이/등하원 1시간 당 금액
            case '3' :
                $standard = $salary_price->care_type_price_1;
                break; // 학습 1시간 당 금액
            case '4' :
                $standard = $salary_price->care_type_price_2;
                break; // 창의미술 1시간 당 금액
            case '5' :
                $standard = $salary_price->care_type_price_3;
                break; // 영어 1시간 당 금액
            default :
                break;
        }

        return $standard * $time;
    }
}